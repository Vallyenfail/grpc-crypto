package grpcService

import (
	"context"

	cservice "gitlab.com/Vallyenfail/grpc-crypto/internal/modules/crypto/service"

	"gitlab.com/Vallyenfail/grpc-crypto/grpc/proto"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/models"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CryptoServiceGRPC struct {
	proto.UnimplementedCryptoServiceRPCServer
	service cservice.Crypter
}

func NewCryptoServiceGRPC(service cservice.Crypter) *CryptoServiceGRPC {
	return &CryptoServiceGRPC{service: service}
}

func (c CryptoServiceGRPC) Update(ctx context.Context, e *emptypb.Empty) (*emptypb.Empty, error) {
	c.service.Update(ctx, models.Coins{})
	return &emptypb.Empty{}, nil
}

func (c CryptoServiceGRPC) Cost(ctx context.Context, e *emptypb.Empty) (*proto.CostResponse, error) {
	out := c.service.Cost(ctx)
	res := make([]*proto.Coin, len(out.Coins))
	for i := range out.Coins {
		res[i] = &proto.Coin{
			Id:       out.Coins[i].ID,
			Name:     out.Coins[i].Name,
			BuyPrice: out.Coins[i].BuyPrice,
		}
	}
	return &proto.CostResponse{
		Coins:     res,
		ErrorCode: 0,
	}, nil
}

func (c CryptoServiceGRPC) History(ctx context.Context, e *emptypb.Empty) (*proto.HistoryResponse, error) {
	out := c.service.History(ctx)
	res := make([]*proto.Coin, len(out.Coins))
	for i := range out.Coins {
		res[i] = &proto.Coin{
			Id:        out.Coins[i].ID,
			Name:      out.Coins[i].Name,
			LastTrade: out.Coins[i].LastTrade,
		}
	}
	return &proto.HistoryResponse{
		Coins: res,
	}, nil
}

func (c CryptoServiceGRPC) Max(ctx context.Context, e *emptypb.Empty) (*proto.MaxResponse, error) {
	out := c.service.Max(ctx)
	res := make([]*proto.Coin, len(out.Coins))
	for i := range out.Coins {
		res[i] = &proto.Coin{
			Id:   out.Coins[i].ID,
			Name: out.Coins[i].Name,
			High: out.Coins[i].High,
		}
	}
	return &proto.MaxResponse{
		Coins: res,
	}, nil
}

func (c CryptoServiceGRPC) Min(ctx context.Context, e *emptypb.Empty) (*proto.MinResponse, error) {
	out := c.service.Min(ctx)
	res := make([]*proto.Coin, len(out.Coins))
	for i := range out.Coins {
		res[i] = &proto.Coin{
			Id:   out.Coins[i].ID,
			Name: out.Coins[i].Name,
			Low:  out.Coins[i].Low,
		}
	}
	return &proto.MinResponse{
		Coins: res,
	}, nil
}

func (c CryptoServiceGRPC) Avg(ctx context.Context, e *emptypb.Empty) (*proto.AvgResponse, error) {
	out := c.service.Avg(ctx)
	res := make([]*proto.Coin, len(out.Coins))
	for i := range out.Coins {
		res[i] = &proto.Coin{
			Id:   out.Coins[i].ID,
			Name: out.Coins[i].Name,
			Avg:  out.Coins[i].Avg,
		}
	}
	return &proto.AvgResponse{
		Coins: res,
	}, nil
}
