package storage

import (
	"context"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/db/adapter"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/cache"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/db/scanner"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/errors"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/models"
	"log"
)

var maxData = map[string]float64{}
var minData = map[string]float64{}

type CryptoStorage struct {
	adapter *adapter.SQLAdapter
	cache   cache.Cache
}

func NewCryptoStorage(adapter *adapter.SQLAdapter, cache cache.Cache) *CryptoStorage {
	return &CryptoStorage{
		adapter: adapter,
		cache:   cache,
	}
}

// TODO: add cache!
func (c *CryptoStorage) Update(ctx context.Context, coins models.Coins) {

	var maxDto models.CryptoMaxDTO
	var minDto models.CryptoMinDTO

	var err error

	var listMax []models.CryptoMaxDTO
	var listMin []models.CryptoMinDTO

	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf("error when trying to retrieve data from table %s", maxDto.TableName())
	}

	err = c.adapter.List(ctx, &listMin, minDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf("error when trying to retrieve data from table %s", minDto.TableName())
	}

	for k, v := range coins {
		i := 0
		var bufferMax models.CryptoMaxDTO
		var bufferMin models.CryptoMinDTO

		if len(listMax) < 1 || len(listMin) < 1 {

			bufferMax.
				SetName(k).
				SetAvg(v.Avg).
				SetUpdated(int(v.Updated)).
				SetBuyPrice(v.BuyPrice).
				SetHigh(v.High).
				SetLastTrade(v.LastTrade).
				SetLow(v.Low).
				SetSellPrice(v.SellPrice).
				SetVol(v.Vol).
				SetVolCurr(v.VolCurr)

			bufferMin.
				SetName(k).
				SetAvg(v.Avg).
				SetUpdated(int(v.Updated)).
				SetBuyPrice(v.BuyPrice).
				SetHigh(v.High).
				SetLastTrade(v.LastTrade).
				SetLow(v.Low).
				SetSellPrice(v.SellPrice).
				SetVol(v.Vol).
				SetVolCurr(v.VolCurr)

			err = c.adapter.Create(ctx, &bufferMax)
			if err != nil {
				log.Fatalf("error when trying to create data at table %s", maxDto.TableName())
			}
			err = c.adapter.Create(ctx, &bufferMin)
			if err != nil {
				log.Fatalf("error when trying to create data at table %s", minDto.TableName())
			}
		} else {
			if v.High > listMax[i].GetHigh() {
				bufferMax.
					SetName(k).
					SetAvg(v.Avg).
					SetUpdated(int(v.Updated)).
					SetBuyPrice(v.BuyPrice).
					SetHigh(v.High).
					SetLastTrade(v.LastTrade).
					SetLow(v.Low).
					SetSellPrice(v.SellPrice).
					SetVol(v.Vol).
					SetVolCurr(v.VolCurr)
				err = c.adapter.Update(ctx, &bufferMax, adapter.Condition{Equal: sq.Eq{"name": k}}, scanner.Update)
				if err != nil {
					log.Fatalf("error when trying to create data at table %s", maxDto.TableName())
				}

				if val, ok := maxData[k]; !ok {
					maxData[k] = bufferMax.GetHigh()
					continue
				} else if ok && bufferMax.GetHigh()-val >= 100 {

					err = RabbitMQ(fmt.Sprintf("максимальная цена для %s: была %f., а стала =  %f.", k, val, bufferMax.GetHigh()))
					if err != nil {
						log.Fatal("error when trying to push notify max value to service")
					}
					maxData[k] = bufferMax.GetHigh()
				}
			}
			if v.Low < listMin[i].GetLow() {
				bufferMin.
					SetName(k).
					SetAvg(v.Avg).
					SetUpdated(int(v.Updated)).
					SetBuyPrice(v.BuyPrice).
					SetHigh(v.High).
					SetLastTrade(v.LastTrade).
					SetLow(v.Low).
					SetSellPrice(v.SellPrice).
					SetVol(v.Vol).
					SetVolCurr(v.VolCurr)
				err = c.adapter.Update(ctx, &bufferMin, adapter.Condition{
					Equal: sq.Eq{"name": k},
				}, scanner.Update)
				if err != nil {
					log.Fatalf(`error when trying to update data at table %v`, minDto)
				}

				if val, ok := minData[k]; !ok {
					minData[k] = bufferMin.GetLow()
					continue
				} else if ok && val-bufferMin.GetLow() >= 100 {

					err = RabbitMQ(fmt.Sprintf("минимальная цена для %s: была %f., а стала =  %f.", k, val, bufferMin.GetLow()))
					if err != nil {
						log.Fatal(`error when trying to push notify min value to service`)
					}
					minData[k] = bufferMin.GetLow()
				}
			}
		}
		i++
	}
}

func (c *CryptoStorage) Cost(ctx context.Context) models.CostOut {
	var maxDto models.CryptoMaxDTO
	var err error

	var listMax []models.CryptoMaxDTO
	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Cost and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return models.CostOut{ErrorCode: errors.CoinServiceRetrieveCostErr}
	}
	var out []models.Coin
	for _, e := range listMax {
		out = append(out, models.Coin{
			ID:       int64(e.GetID()),
			Name:     e.GetName(),
			BuyPrice: e.GetBuyPrice(),
		})
	}
	return models.CostOut{
		Coins:     out,
		ErrorCode: 0,
	}
}

func (c *CryptoStorage) History(ctx context.Context) models.HistoryOut {
	var maxDto models.CryptoMaxDTO
	var err error

	var listMax []models.CryptoMaxDTO
	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method History and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return models.HistoryOut{ErrorCode: errors.CoinServiceRetrieveHistoryErr}
	}
	var out []models.Coin
	for _, e := range listMax {
		out = append(out, models.Coin{
			ID:        int64(e.GetID()),
			Name:      e.GetName(),
			LastTrade: e.GetLastTrade(),
		})
	}
	return models.HistoryOut{
		Coins:     out,
		ErrorCode: 0,
	}
}

func (c *CryptoStorage) Max(ctx context.Context) models.MaxOut {
	var maxDto models.CryptoMaxDTO
	var err error

	var listMax []models.CryptoMaxDTO
	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Max and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return models.MaxOut{
			ErrorCode: errors.CoinStorageRetrieveMaxErr,
		}
	}
	var result []models.Coin
	for _, e := range listMax {
		result = append(result, models.Coin{
			ID:   int64(e.GetID()),
			Name: e.GetName(),
			High: e.GetHigh(),
		})
	}

	return models.MaxOut{
		Coins:     result,
		ErrorCode: 0,
	}
}

func (c *CryptoStorage) Min(ctx context.Context) models.MinOut {
	var minDto models.CryptoMinDTO
	var err error

	var listMin []models.CryptoMinDTO
	err = c.adapter.List(ctx, &listMin, minDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Min and table %v`, minDto)
	}
	if len(listMin) == 0 {
		return models.MinOut{
			ErrorCode: errors.CoinStorageRetrieveMinErr,
		}
	}
	var result []models.Coin
	for _, e := range listMin {
		result = append(result, models.Coin{
			ID:   int64(e.GetID()),
			Name: e.GetName(),
			Low:  e.GetLow(),
		})
	}

	return models.MinOut{
		Coins:     result,
		ErrorCode: 0,
	}
}

func (c *CryptoStorage) Avg(ctx context.Context) models.AvgOut {
	var maxDto models.CryptoMaxDTO
	var err error

	var listMax []models.CryptoMaxDTO
	err = c.adapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Avg and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return models.AvgOut{
			ErrorCode: errors.CoinStorageRetrieveAvgErr,
		}
	}
	var result []models.Coin
	for _, e := range listMax {
		result = append(result, models.Coin{
			ID:   int64(e.GetID()),
			Name: e.GetName(),
			Avg:  e.GetAvg(),
		})
	}

	return models.AvgOut{
		Coins:     result,
		ErrorCode: 0,
	}
}
