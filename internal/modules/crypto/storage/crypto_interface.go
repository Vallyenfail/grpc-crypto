package storage

import (
	"context"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/models"
)

type Crypter interface {
	Update(ctx context.Context, models models.Coins)
	Cost(ctx context.Context) models.CostOut
	History(ctx context.Context) models.HistoryOut
	Max(ctx context.Context) models.MaxOut
	Min(ctx context.Context) models.MinOut
	Avg(ctx context.Context) models.AvgOut
}
