package service

import (
	"context"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/models"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/modules/crypto/storage"
	"go.uber.org/zap"
)

type CryptoService struct {
	storage storage.Crypter
	logger  *zap.Logger
}

func NewCryptoService(storage storage.Crypter, logger *zap.Logger) *CryptoService {
	return &CryptoService{
		storage: storage,
		logger:  logger,
	}
}

func (c *CryptoService) Update(ctx context.Context, models models.Coins) {
	c.storage.Update(ctx, models)
}

func (c *CryptoService) Cost(ctx context.Context) models.CostOut {
	out := c.storage.Cost(ctx)
	return out
}

func (c *CryptoService) History(ctx context.Context) models.HistoryOut {
	out := c.storage.History(ctx)
	return out
}

func (c *CryptoService) Max(ctx context.Context) models.MaxOut {
	out := c.storage.Max(ctx)
	return out
}

func (c *CryptoService) Min(ctx context.Context) models.MinOut {
	out := c.storage.Min(ctx)
	return out
}

func (c *CryptoService) Avg(ctx context.Context) models.AvgOut {
	out := c.storage.Avg(ctx)
	return out
}
