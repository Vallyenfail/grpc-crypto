package run

import (
	"context"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/Vallyenfail/grpc-crypto/config"
	grpcService "gitlab.com/Vallyenfail/grpc-crypto/grpc"
	"gitlab.com/Vallyenfail/grpc-crypto/grpc/proto"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/db"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/cache"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/component"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/db/migrate"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/db/scanner"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/errors"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/responder"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/server"
	internal "gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/service"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/infrastructure/tools/cryptography"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/models"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/modules"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/provider"
	"gitlab.com/Vallyenfail/grpc-crypto/internal/storages"
	rpc2 "gitlab.com/Vallyenfail/grpc-crypto/rpc"
	"gitlab.com/Vallyenfail/grpc-crypto/worker"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net/rpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	GRPC     server.Server
	jsonRPC  server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Servises *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	switch a.conf.RPCServer.Type {

	case `jrpc`:
		//запускаем rpc сервер
		errGroup.Go(func() error {
			err := a.jsonRPC.Serve(ctx)
			if err != nil {
				a.logger.Error("app: RPC server error", zap.Error(err))
				return err
			}
			return nil
		})

		if err := errGroup.Wait(); err != nil {
			return errors.GeneralError
		}
		return errors.NoError

	case `grpc`:
		//запускаем Grpc сервер
		errGroup.Go(func() error {
			err := a.GRPC.Serve(ctx)
			if err != nil {
				a.logger.Error("app: GRPC server error", zap.Error(err))
				return err
			}
			return nil
		})

		if err := errGroup.Wait(); err != nil {
			return errors.GeneralError
		}

		return errors.NoError
	}

	return errors.NoError

}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.CryptoMinDTO{},
		&models.CryptoMaxDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages

	// инициализация сервисов
	services := modules.NewServices(newStorages, components)
	w := worker.NewCryptoUpdater(services)
	w.Run()
	a.Servises = services
	switch a.conf.RPCServer.Type {
	case "grpc":
		grpcServer := grpc.NewServer()
		cryptoGrpc := grpcService.NewCryptoServiceGRPC(services.Crypto)
		proto.RegisterCryptoServiceRPCServer(grpcServer, cryptoGrpc)
		a.GRPC = server.NewGRPCServer(a.conf.RPCServer, grpcServer, a.logger)
	case "jrpc":
		cryptoRPCClient := rpc2.NewCryptoRPCService(services.Crypto)
		rpcServer := rpc.NewServer()
		err = rpcServer.Register(cryptoRPCClient)
		if err != nil {
			a.logger.Fatal("error init crypto json RPC", zap.Error(err))
		}
		a.jsonRPC = server.NewJSONRPC(a.conf.RPCServer, rpcServer, a.logger)
	}

	return a
}
